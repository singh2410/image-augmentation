#!/usr/bin/env python
# coding: utf-8

# # Pet Finder Model: Image Augmentation
# #By- Aarush Kumar
# #Dated:  October 26,2021

# In[1]:


get_ipython().system('pip install wandb')


# In[2]:


get_ipython().system('pip install torchvision')


# In[5]:


get_ipython().system('pip install transformers')


# In[7]:


pip install -U albumentations


# In[8]:


import gc
import os
import glob
import sys
import cv2
import imageio
import joblib
import math
import random
import wandb
import math

import numpy as np
import pandas as pd

from scipy.stats import kstest

import matplotlib.pyplot as plt
import matplotlib.patches as patches
import matplotlib.image as mpimg

from PIL import Image

from statsmodels.graphics.gofplots import qqplot

plt.rcParams.update({'font.size': 18})
plt.style.use('fivethirtyeight')

import seaborn as sns
import matplotlib

from termcolor import colored

from multiprocessing import cpu_count
from tqdm.notebook import tqdm
from sklearn.model_selection import StratifiedKFold
from scipy.stats import pearsonr

import torch
import transformers
import torch.nn as nn
import torch.nn.functional as F
from torch.utils.data import DataLoader, Dataset
import torchvision
import torchvision.transforms as transforms

from sklearn.model_selection import StratifiedKFold
from sklearn.metrics import r2_score, mean_squared_error

from albumentations import (
    HorizontalFlip, VerticalFlip, IAAPerspective, ShiftScaleRotate, CLAHE, RandomRotate90,
    Transpose, ShiftScaleRotate, Blur, OpticalDistortion, GridDistortion, HueSaturationValue,
    IAAAdditiveGaussianNoise, GaussNoise, MotionBlur, MedianBlur, IAAPiecewiseAffine, RandomResizedCrop,
    IAASharpen, IAAEmboss, RandomBrightnessContrast, Flip, OneOf, Compose, Normalize, Cutout, CoarseDropout, ShiftScaleRotate, CenterCrop, Resize
)
from albumentations.pytorch import ToTensorV2

import warnings
warnings.simplefilter('ignore')

# Activate pandas progress apply bar
tqdm.pandas()


# In[9]:


class config:
    DIRECTORY_PATH = "/home/aarush100616/Downloads/Projects/Pet Finder model"
    TRAIN_FOLDER_PATH = DIRECTORY_PATH + "/train"
    TRAIN_CSV_PATH = DIRECTORY_PATH + "/train.csv"
    TEST_CSV_PATH = DIRECTORY_PATH + "/test.csv"
    SEED = 42


# In[10]:


WANDB_CONFIG = {
     'competition': 'PetFinder', 
              '_wandb_kernel': 'neuracort'
    }


# In[11]:


def set_seed(seed=config.SEED):
    random.seed(seed)
    os.environ["PYTHONHASHSEED"] = str(seed)
    np.random.seed(seed)
    torch.manual_seed(seed)
    torch.cuda.manual_seed(seed)
    torch.cuda.manual_seed_all(seed)
    torch.backends.cudnn.deterministic = True
    torch.backends.cudnn.benchmark = False
set_seed()


# In[12]:


def return_filpath(name, folder=config.TRAIN_FOLDER_PATH):
    path = os.path.join(folder, f'{name}.jpg')
    return path


# In[13]:


train = pd.read_csv(config.TRAIN_CSV_PATH)
train['image_path'] = train['Id'].apply(lambda x: return_filpath(x))


# In[14]:


train.head()


# In[15]:


train.dtypes


# In[16]:


train.describe()


# In[17]:


train.describe().T


# In[47]:


train.dtypes


# In[18]:


def plot_augments(title, color_space):
    # Define subplots
    fig, axes = plt.subplots(nrows=2, ncols=6, figsize=(16,6))
    plt.suptitle(title, fontsize = 16)
    # Loop through images
    for i in range(0, 2*6):
        image = cv2.imread(train['image_path'][i])
        # The function converts an input image from one color space to another.
        image = cv2.cvtColor(image, color_space)
        image = cv2.resize(image, (200,200))
        x = i // 6
        y = i % 6
        axes[x, y].imshow(image, cmap=plt.cm.bone) 
        axes[x, y].axis('off')


# In[19]:


plot_augments("Original", cv2.COLOR_BGR2RGB)


# In[20]:


plot_augments("B&W", cv2.COLOR_RGB2GRAY)


# In[21]:


plot_augments("Without Gaussian Blur", cv2.COLOR_RGB2HSV)


# In[22]:


plot_augments("With Gaussian Blur", cv2.COLOR_RGB2HSV)


# In[23]:


plot_augments("Hue, Saturation, Brightness", cv2.COLOR_RGB2HLS)


# In[24]:


plot_augments("LUV Color Space", cv2.COLOR_RGB2LUV)


# In[25]:


plot_augments("Aplha Channel", cv2.COLOR_RGB2RGBA)


# In[26]:


plot_augments("CIE XYZ", cv2.COLOR_RGB2XYZ)


# In[27]:


plot_augments("Luma Chroma", cv2.COLOR_RGB2YCrCb)


# In[28]:


plot_augments("CIE Lab", cv2.COLOR_RGB2Lab)


# In[29]:


plot_augments("YUV Color Space", cv2.COLOR_RGB2YUV)


# In[30]:


# Select a small sample of the image paths
image_list = train.sample(12)['image_path']
image_list = image_list.reset_index()['image_path']
# Show the sample
plt.figure(figsize=(16,6))
plt.suptitle("Original View", fontsize = 16)
for k, path in enumerate(image_list):
    image = mpimg.imread(path)
    plt.subplot(2, 6, k+1)
    plt.imshow(image)
    plt.axis('off')


# In[31]:


class PetDataset(Dataset):
    def __init__(self, image_list, transforms = None):
        self.image_list = image_list
        self.transforms = transforms
    def __len__(self):
        return len(self.image_list)
    def __getitem__(self, i):
        image = plt.imread(self.image_list[i])
        image = Image.fromarray(image).convert('RGB')
        image = np.asarray(image).astype(np.uint8)
        if self.transforms is not None:
            image = self.transforms(image)
        return torch.tensor(image, dtype = torch.float)


# In[32]:


# Function to display applied transformations
def show_transform(image, title):
    plt.figure(figsize = (16,6))
    plt.suptitle(title, fontsize = 16)
    #Unnormalize
    image = image / 2 + 0.5
    npimg = image.numpy()
    npimg = np.clip(npimg, 0., 1.)
    plt.imshow(np.transpose(npimg, (1, 2, 0)))
    plt.show()   


# In[33]:


def apply_transform(transform_layer, title):
    transform = transforms.Compose(
        [
            transforms.ToPILImage(),
            transforms.Resize((300 , 300)),
            transform_layer,
            transforms.ToTensor(),
            transforms.Normalize((0.5, 0.5, 0.5), (0.5, 0.5, 0.5))
        ])
    # Dataset
    train_dataset = PetDataset(image_list = image_list, transforms = transform)
    # DataLoader
    train_dataloader = DataLoader(dataset = train_dataset, batch_size = 12, shuffle = True)
    # Select Data
    images = next(iter(train_dataloader))
    # Show Images
    show_transform(torchvision.utils.make_grid(images, nrow = 6), title = title)


# In[34]:


apply_transform(transforms.CenterCrop((100, 100)), "Center Crop")


# In[35]:


apply_transform(transforms.RandomCrop((100, 100)), "Random Crop")


# In[36]:


apply_transform(transforms.RandomResizedCrop(size = 60), "Random Resized Crop")


# In[37]:


apply_transform(transforms.ColorJitter(brightness=0.7, contrast=0.7, saturation=0.7, hue=0.5), "Color Jitter")


# In[38]:


apply_transform(transforms.Pad(padding = 5), "Padding")


# In[39]:


apply_transform(transforms.RandomAffine(degrees=45), "Random Affine")


# In[40]:


apply_transform(transforms.RandomHorizontalFlip(p=0.7), "Random Horizontal Flip")


# In[41]:


apply_transform(transforms.RandomVerticalFlip(p=0.7), "Random Vertical Flip")


# In[42]:


apply_transform(transforms.RandomPerspective(p=0.7), "Random Perspective")


# In[43]:


apply_transform(transforms.RandomRotation(degrees = 180), "Random Rotation")


# In[44]:


apply_transform(transforms.RandomInvert(p=0.7), "Random Invert")


# In[46]:


apply_transform(transforms.RandomPosterize(bits = 4, p=0.7), "Random Posterize")


# In[ ]:




